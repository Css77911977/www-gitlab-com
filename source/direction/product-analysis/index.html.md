---
layout: markdown_page
title: Product Direction - Product Analysis
canonical_path: "/direction/product-analysis/"
---

## Product Analysis Group Overview

The Product Analysis group consists of a team of product analysts. This group reports to the Director of Product, Growth and serves as a specialized analytics team to support product data analysis requests. The primary customer this group serves is Product Managers from various sections/groups. 

The Product Analysis group is part of the [Research & Development (R&D) Data Fusion Team](/handbook/business-technology/data-team/#data-fusion-teams) where the product analysts work closely with members from the central data team.

## Responsibilities

Since the Product Analysis group is a specialized analytics team, it collaborates with the [Data team](/handbook/business-technology/data-team/) and [Product Intelligence group](/direction/product-intelligence/) very closely. Here is a quick summary of the responsbilities of each of these 3 teams: 

| Team Name | DRI | Goal | Example Tasks |
| ---------- | ---------- |-------------- |-------------- |
|Product Analysis group|Carolyn Braza: Manager, Product Data Analysis| Perform analysis to unlock product insights when the data is available| Ad hoc product analysis, experiment analysis, Product KPI tracking, and creation and maintenance of standardized Product metric dashboards|
|Product Intelligence group|Amanda Rueda: Senior Product Manager, Product Intelligence (acting)| Define and improve capabilities of product data set|Make sure our product data set is comprehensive, available, and accurate,and that team members are enabled to create new instrumentation, charts, and dashboards in a self-service fashion|
|Data Team|Rob Parker, Senior Director, Data and Analytics| Build and maintain product data models and infrastructure to support data analysis|Create repeatable and reliable product data sets & models & related data pipelines/processes|

Note that due to the strategic importance of product data analysis and how early we are in the journey, the data team and product analysis team formed the R&D Fusion Team. The fusion team will take on some shared responsibilities in data analysis, data modeling and knowledge transfer/training for the foreseeable future, to progress our product data capabilities.

For more information about how the Product Analysis group works with the Data team at GitLab, please refer to following the model below and the [data team's handbook page](/handbook/business-technology/data-team/#how-data-teams-work-together).
 
### High level Collaboration Workflow between the 3 teams
1. A data question needs to be answered with product data, the requester opens a data issue
2. The data triager decides whether we have the data avaliable to answer that question  
3. If we don't have the data, open an issue for Product Intelligence Group
4. If we have the data, route the requests to the Product Analysis Group
5. Product Analysts perform analysis and build draft charts or dashboards
6. Product Analysts define if the data analysis needs to be repeatable
7. If the data analysis needs to repeatable, open an issue for data team to create trusted data model to enable future analysis

## Issue intake and prioritization

You can read about the Product Analysis group's team processes on their [handbook page](/handbook/product/product-analysis/).

## Product Analysis Group’s Current Focus Area

As the Product Analysis Group was established in FY 21 Q4, the team is still in the process of forming. In FY22 Q1 and Q2, we will focus on the following areas: 

### Analysis Tasks 
* Knowledge transfer from the Data team to the Product Analysis group on product KPIs such as SpO and xMAUs
* Work with the Data team to understand and improve data models powering product KPIs 
* Perform ad-hoc analysis to support product analysis requests, including: 
  - Product KPIs: SpO & xMAU
  - Product Key Review & executive/OKR support
  - Growth Experiments Analysis & New User Conversion Analysis

### Team and Process
* Hire for the open positions and get new product analysts onboarded 
* Understand team velocity and establish and improve the prioritization process 
* Establish collaboration process with the data team and product intelligence group 

## Helpful resources & links
1. [Growth Data Guide](/handbook/product/product-analysis/growth-data-guide/)
1. [Data for product managers](/handbook/business-ops/data-team/programs/data-for-product-managers/)
1. [Data team How we can help you Page](/handbook/business-ops/data-team/#how-we-can-help-you)
